$(window).load(function()
    {
AddCallbackEvent('StimulusAndQuestionLoaded', function(){
});

        AddCallbackEvent('StimulusAndQuestionLoaded', initUnit);
        
        function initUnit(params, moduleId)
        {

			var itemNumber = getItemNumber();
			var curItemNumber = 0;
			var selectedWord = null;
			var itemStartTime = getGlobalTime().item;
			
			$('.sentenceAnswer').click(function(e) {
				if(selectedWord != null) {
					$('#' + selectedWord).removeClass('selectedWord');
				}
				
				selectedWord = $(this).attr('id');
				$(this).addClass('selectedWord');
				feedtrace('sentence', 'selectWord', selectedWord);
				
				{
					var items = $('.sentenceItem');
					
					if ($('.sentenceProcAnswers .selectedWord').length == 0 && !$('#noAnswerMsg').hasClass('shownItem')) {
						$('#noAnswerMsg').addClass('shownItem');
						return true;
					}
					
					// NEED TO SAVE RESPONSE HERE
					var itemEndTime = getGlobalTime().item;
					var varName = "SentenceData_" + getCurrentUnitID() + "_" + getCurrentItemID();
					var sentenceData = getUserVar(varName);
					if (sentenceData == null)
						sentenceData = {};
					var yesKey = $(items[curItemNumber]).hasClass('yesKey');
					var itemCorrect = (yesKey && $('#AnswerYes').hasClass('selectedWord')) || 
										(!yesKey && $('#AnswerNo').hasClass('selectedWord'));
					sentenceData[curItemNumber] = {
						itemID: $(items[curItemNumber]).attr('id'),
						selected: $('.selectedWord').attr('id'),
						correct: itemCorrect,
						time: itemEndTime - itemStartTime,
					};
	
					setUserVar(varName, sentenceData);

					curItemNumber++;
					selectedWord = null;

					if (curItemNumber < items.length) {
						window.setTimeout(function(e) {
							$('#noAnswerMsg').removeClass('shownItem');
							$('.activeItem').removeClass('activeItem');
							$('.sentenceAnswer').removeClass('selectedWord');
							$(items[curItemNumber]).addClass('activeItem');
							$('#container').css('visibility','visible');
							setCounts(curItemNumber+1, items.length);
							itemStartTime = getGlobalTime().item;
							return true;
						}, 250);
					} else {
						$('#container').css('visibility','hidden');
						$('#endMsg').css('display', 'block');
						__disableNav = false;
						getControler().enableNavigation();
						forward();
					}
				}
			});
			
			disableNav();
			$($('.sentenceItem')[0]).addClass('activeItem');
			setCounts(1, $('.sentenceItem').length);
        }

});

var __disableNav = true;
function disableNav() {
	try {
		if (__disableNav) getControler().disableNavigation();
	} catch (e) {
		window.setTimeout(disableNav, 1000);
	}
}

function setCounts(num, total) {
	__naxControler.GetNaxContext('question').container[0].contentWindow.$('#item')[0].contentWindow.$('#qNumber').text(num);
	__naxControler.GetNaxContext('question').container[0].contentWindow.$('#item')[0].contentWindow.$('#qTotal').text(total);
}
