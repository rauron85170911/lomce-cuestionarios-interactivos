
var NaxControl = (function(){
    var _instance = null;//reference to public part

    var _me = this;//to keep reference to private part

    var _model = null;

    var countedActions = "click,keypress,dblclick,drop";
    var actionsCount = 0;

    var BASE_URL = window.location.href.split('index.html')[0];

    var naxEvents = {};
	
    var heldEvents = new Array();
    var holdEvents = false;
	
    var eventCounter = 0;

    var platformReady = false;//boolean set to false when pages are loading, true otherwise

    var unitModulesInstancied = null;
    var itemModulesInstancied = null;

    var staticModules = {};//modules always loaded on any unit
    //var staticModules = {tao: {name: 'tao', id: 'tao', window: null}};//modules always loaded on any unit

    var testFramesLoadedInterval = null;

    var StimulusQuestionModules = {
        stimulus: {
            frame:null, 
            window:null
        }, 
        question: {
            frame:null, 
            window:null
        }
    };

var moduleStored_object = {};
var frameStored_array = new Array();
    var framesCount = 0;

    var question_frames = new Array();
    var stimulus_frames = new Array();

    var questionLoaded = false;
    var stimulusLoaded = false;
	
    var timeFirstAction = 0;
	
    var selectedText = null;
    var focusedField = null;

    var NaxControl = function(model_){
        /*private properties*/
        _model = model_;

        naxEvents.onItemBegin = new Array();
        naxEvents.onItemEnd = new Array();
        naxEvents.onNextClicked = new Array();

        unitModulesInstancied = {};
        itemModulesInstancied = {};

        /*private functions*/

        /*
		module : module object
		context : item or unit
		callback : function called when the module is loaded
		*/
        var loadModule = function(module, context, callback, container)
        {
            switch (context)//return instance if already exists
            {
                case 'unit':
                {
                    if ((module.id in unitModulesInstancied) && (unitModulesInstancied[module.id]))
                    {
						//console.log("LoadModule: Found unit scope " + module.id);	//DEBUG
                        return unitModulesInstancied[module.id];
                    }
                    break;
                }
                case 'item':
                {
                    if ((module.id in itemModulesInstancied) && (itemModulesInstancied[module.id]))
                    {
						//console.log("LoadModule: Found item scope " + module.id);	//DEBUG
                        return itemModulesInstancied[module.id];
                    }
                    break;
                }
                default:
                {
                    console.log('context module error : '+context);
                    return;
                }
            }
            var iframe = $('<iframe>');
            iframe.bind('load', {
                moduleParams: module.params, 
                context: context
            }, moduleLoaded);

            if (typeof(module.id) != 'undefined')
            {
                iframe.attr('id', module.id);
            }
            var lang = _model.GetLang();
            if (typeof(module.lang) != 'undefined')
            {
                lang = module.lang;
            }
            var moduleExistInLang = _model.IsResourceAvailableInLanguage( module.module, lang, 'module');
            if (! moduleExistInLang)
            {
                var reg = new RegExp('^([a-z]{3}-[A-Z]{3}).+$');
                var result = reg.exec(lang);
                //if lang is as xx-XX_ZZZZZZ, try to get the xx-XX lang if is available
                if(result != null)
                {
                    lang = result[1];
                    moduleExistInLang = _model.IsResourceAvailableInLanguage( module.module, lang, 'module');
                    if (! moduleExistInLang)
                    {
                        lang = 'eng-ZZZ';
                    }
                }
                else
                {
                    lang = 'eng-ZZZ';
                }
            }
            if (typeof(callback) == 'function')
            {
                iframe.bind('load', function(e){
                    try{
                        callback(e.target.contentWindow);
                    }catch(e){}
                });
        }
		//console.log("loadmodule: setting iframe src for " + module.id);	//DEBUG
        iframe.attr('src', config['PATH_MODULE'] + '/'+module.module+'/'+lang+ '?__id='+module.id+'&__naxTime=' + (new Date).getTime());
        if (typeof(container) == 'undefined')
        {
            $('#naxContainer').append(iframe);
        }
        else
        {
            container.append(iframe);
        }
        switch (context)
        {
            case 'unit':
            {
                unitModulesInstancied[module.id] = {
                    frame: iframe, 
                    module:module
                };
 				//console.log("nax: unitModulesInstancied " + module.id);	//DEBUG
                break;
            }
            case 'item':
            {
                itemModulesInstancied[module.id] = {
                    frame: iframe, 
                    module:module
                };
				//console.log("nax: itemModulesInstancied " + module.id);		//DEBUG
                break;
            }
            default:
            {
                console.log('context module error : '+context);
            }
        }
        testFramesLoaded();//to remove transition screen if all loaded
        return {
            frame: iframe, 
            window: null
        };
    };
    var isModuleLoaded = function(id, context)
    {
        var found = false;
        switch(context)
        {
            case 'unit':
            {
                found = (typeof(unitModulesInstancied[id]) != 'undefined');
                break;
            }
            case 'item':
            {
                found = (typeof(itemModulesInstancied[id]) != 'undefined');
                break;
            }
            default:
            {
                }
        }
        return found;
    };
    var unLoadModule = function(module)
    {
        switch(module.naxContext.context)
        {
            case 'unit':
            {
                delete unitModulesInstancied[module.__id];
				//console.log("nax: delete unitModulesInstancied " + module.__id);	//DEBUG
                break;
            }
            case 'item':
            {
                delete itemModulesInstancied[module.__id];
				//console.log("nax: delete itemModulesInstancied " + module.__id);	//DEBUG
                break;
            }
            default:
            {
                console.log('context module error : '+context);
            }
        }
        $('#'+module.__id).empty();
        $('#'+module.__id).remove();
    };
    var frameReady = new Array();
    /*show modules of platform level, item scope*/
    var ShowModulesItem = function(modules)
    {
        for (var i in itemModulesInstancied)//for each instancied modules
        {
            itemModulesInstancied[i].frame.remove();//remove jquery element from dom
            delete itemModulesInstancied[i];//remove from instancied modules
			//console.log("nax: delete itemModulesInstancied " + i);	//DEBUG
            delete moduleStored_object[i];
			//console.log("nax: delete moduleStored_object " + i);	//DEBUG
        }
        platformReady = false;
        if (modules)
        {
            for (var i in modules)
            {
                var iframe = loadModule(modules[i], 'item');
            }
        }
    };
    var ShowModulesUnit = function()	/*show modules of platform level, unit scope, called on start public function*/
    {
        var modules = _model.GetModule().unit;
        for (var i in modules)
        {
            if (typeof(unitModulesInstancied[modules[i].id]) == 'undefined' )
            {
                var iframe = loadModule(modules[i], 'unit');
                unitModulesInstancied[modules[i].id] = {
                    frame: iframe.frame, 
                    module:modules[i]
                    };
				//console.log("nax: unitModulesInstancied " + modules[i].id);	//DEBUG
            }
        }
        for (var mod in staticModules)
        {
            loadModule({
                module: mod, 
                id: staticModules[mod].id, 
                params:null
            },'unit').window;
        }

        insertPrefetchLinks();
    };
    var getBubblingEvents = function (data)
    {
        var EVENTS = {
            bubbling:[],
            nonBubbling:[]
        };
        if ((typeof(data.type) != 'undefined') && (data.type.length > 0))
        {
            var list = data.value.split(',');
				
            for (i in list)
            {
                if ($.inArray(list[i],['click', 'dblclick', 'change', 'submit', 'select', 'mousedown', 'mouseup', 'mouseenter', 'mousemove', 'mouseout', 'keypress', 'keyup', 'keydown','drop']) > -1)//if is bubbling event
                {
                    EVENTS.bubbling.push(list[i]);
                }
                else
                {
                    EVENTS.nonBubbling.push(list[i]);// else non bubbling event
                }
            }
        }
        else
        {
            EVENTS = {
                bubbling:['click', 'dblclick', 'change', 'submit', 'select', 'mousedown', 'mouseup', 'mouseenter', 'mousemove', 'mouseout', 'keypress', 'keyup', 'keydown','drop'], 
                nonBubbling:['blur', 'focus', 'load', 'resize', 'scroll', 'unload', 'beforeunload', 'select', 'submit']
                };
        }
        return EVENTS;
    };
    var ShowItem = function()
    {
        /*
				called on load event and on Next call
			*/
        updatePlatformCss();
        platformReady = false;
        ReinitializeActioncounter();
        var mods = _model.GetModule().item;
        ShowModulesItem(mods);

        var moduleStimulus =  {
            id:'stimulus', 
            module: 'item'
        };
        var stimulusObject = _model.GetCurrentStimulus();
        var UnitAvailableInLanguage = _model.IsResourceAvailableInLanguage( _model.GetCurrentUnitID(), _model.GetLang(), 'unit');
        if (! UnitAvailableInLanguage)
        {
            loadModule({
                'id':'transition', 
                'module':'transition'
            }, 'unit', function(moduleWindow)//hide platform

            {
                triggerEvent('onItemEnd');
            /*alert('Preview is not yet available for this unit as a translation has not been uploaded yet.');*/
            });
            return false;
        }
        if (StimulusQuestionModules.stimulus.window == null )
        {
            stimulusLoaded = false;
            StimulusQuestionModules.stimulus = loadModule(moduleStimulus, 'unit', function(moduleWindow)
            {
                try
                {
                    moduleWindow.ItemApi.reloadItem(stimulusObject.url, stimulusObject.reload);
                }
                catch(e)
                {
                    eval.call(moduleWindow,'ItemApi.reloadItem(stimulusObject.url, stimulusObject.reload );');
                }
            });
        }
        else
        {
            stimulusLoaded = false;
            if (! stimulusObject.reload)
            {
                //not reloaded -> do not wait for load event, directly set to true
                stimulusLoaded = true;
            }
            StimulusQuestionModules.stimulus.window.ItemApi.reloadItem(stimulusObject.url, stimulusObject.reload);
        }

        var moduleQuestion = {
            id:'question', 
            module: 'item'
        };
        var questionObject = _model.GetCurrentQuestion();
        if (StimulusQuestionModules.question.window == null)
        {
            StimulusQuestionModules.question = loadModule(moduleQuestion, 'unit', function(moduleWindow)
            {
                if (questionObject.reload)
                {
                    questionLoaded = false;
                }
                else
                {
                    questionLoaded = true;
                }
                try
                {
                    moduleWindow.ItemApi.reloadItem(questionObject.url, questionObject.reload);
                }
                catch(e)
                {
                    eval.call(moduleWindow,'ItemApi.reloadItem(questionObject.url, questionObject.reload);');
                }
            });
        }
        else
        {
            questionLoaded = false;
            StimulusQuestionModules.question.window.ItemApi.reloadItem(questionObject.url, questionObject.reload);
        }

        if (!  _model.GetCurrentStimulus().reload)
        {
            framesCount = framesCount - 1;
        }
        if (! _model.GetCurrentQuestion().reload)
        {
            framesCount = framesCount - 1;
        }

        testFramesLoadedInterval = setInterval(testFramesLoadedIntervalCallback, 1000);//if no new frame/module loaded, manual call to update transition module

        _model.SynchronizeGlobalTime();
    };

    var insertPrefetchLinks = function() {
        var files = _model.GetAllFiles();
        var seen = {};
        var head = $('head');
        $.each(files, function(k,v) {
            if (!seen[v.url]) {
                var mod = '';
                if (v.type) 
                    mod = '?ModuleId=' + v.type;
                $(head).append('<link rel="prefetch" href="' + v.url + mod + '">');
                seen[v.url] = true;
            }
        });
    };
		
    var testFramesLoadedIntervalCallback = function()
    {
        if (platformReady)
        {
            clearInterval(testFramesLoadedInterval);
            testFramesLoadedInterval = null;
        }
        testFramesLoaded();
    };
		
    var updatePlatformCss = function()
    {
        var css = _model.GetCSS();
        var cssPath = css.platform;
        var cssNode = $('#platformCss');
        if (cssPath != cssNode.attr('href'))
        {
            cssNode.attr('href', cssPath);
        }
        var cssLocalePath = css.locale;
        var cssLocaleNode = $('#platformLocaleCss');
        if (cssLocalePath != cssLocaleNode.attr('href'))
        {
            cssLocaleNode.attr('href', cssLocalePath);
        }
    };

    var setPlatformReady = function()
    {
		//console.log("[naxControl] platformReady is true");	//DEBUG
        triggerEvent('onItemBegin');
        platformReady = true;
    };
    var isPlatformReady = function()
    {
        return platformReady;
    };
    var iframesSynchronysed = new Array();//contains all frames from the modules recursively (but not modules) [{iframe, module}, ...]
    var moduleLoaded = function(e)
    {
        if (! platformReady)
        {
            testFramesLoaded();//module loaded by manifest
        }
        /*else
			{*/
        //this is the binding for modules
        bindEventsTracer_module($(e.target).contents().find('body'));//module loaded by api (not synchronized)
    //}
    };
    var iframeLoaded = function(e)
    {
        $(e.target).unbind('load', iframeLoaded);
        bindEventsTracer($(e.target).contents().find('body'));
        testFramesLoaded();
    };
    var testFramesLoaded = function()
    {
        /*
			test 2 array for counting
			frameStored_array
			moduleStored_object
			*/
        var allLoaded = false;
		//console.log("[nax] moduleStored_object: " + util.objectSize(moduleStored_object) + ", unitModulesInstancied: " + util.objectSize(unitModulesInstancied) + ", itemModulesInstancied: " + util.objectSize(itemModulesInstancied));	//DEBUG
        if (util.objectSize(moduleStored_object) >= util.objectSize(unitModulesInstancied)+util.objectSize(itemModulesInstancied))
        {
            //testing modules loaded
            allLoaded = true;
        }
        /*if (allLoaded)
			{
				//testing pages (stimulus, question) loaded
				allLoaded = (frameStored_array.length >= framesCount);
			}*/
			
        if (allLoaded)
        {
            if (!platformReady)
            {
                if (testFramesLoadedInterval != null)
                {
                    clearInterval(testFramesLoadedInterval);
                    testFramesLoadedInterval = null;
                }
                setPlatformReady();
                bindEventsTracer();
            }
        }
    };
    var bindEventsTracer_module = function(element)
    {
        var events = _model.GetEvent();
        var func;
        var attributes = null;

        if ((typeof(events.all) == 'undefined') && (typeof(events.item) != 'undefined') && (typeof(events.stimulus) != 'undefined') && (typeof(events.question) != 'undefined'))
        {
            bind_platform(element, getBubblingEvents({
                type: ''
            }),{});//eventLogger stores dom events
        }
        else
        {
            if (typeof(events.all) != 'undefined')
            {
                attributes = {
                    attributes: events.all.attributes.split(','), 
                    factorize: events.all.factorize
                    };
                if (events.all.list.type == 'catch')
                {
                    func = bind_platform;
                }
                else
                {
                    func = unbind_platform;
                }
                func(element, getBubblingEvents(events.all.list), attributes);//eventLogger stores dom events
            }
            else
            {
                bind_platform(element, getBubblingEvents({
                    type:'', 
                    value:''
                }), attributes);//eventLogger stores dom events
            }
            if (typeof(events.item) != 'undefined')
            {
                attributes = {
                    attributes: events.item.attributes.split(','), 
                    factorize: events.item.factorize
                    };
                if (events.item.list.type == 'catch')
                {
                    func = bind_platform;
                }
                else
                {
                    func = unbind_platform;
                }
                func(element, getBubblingEvents(events.item.list), attributes);//eventLogger stores dom events
            }
            if (typeof(events.stimulus) != 'undefined')
            {
                attributes = {
                    attributes: events.stimulus.attributes.split(','), 
                    factorize: events.stimulus.factorize
                    };
                if (events.stimulus.list.type == 'catch')
                {
                    func = bind_platform;
                }
                else
                {
                    func = unbind_platform;
                }
                func(element, getBubblingEvents(events.stimulus.list), attributes);//eventLogger stores dom events
            }
        /*
				if (typeof(events.question) != 'undefined')
				{
					attributes = {attributes: events.question.attributes.split(','), factorize: events.question.factorize};
					if (events.question.list.type == 'catch')
					{
						func = bind_platform;
					}
					else
					{
						func = unbind_platform;
					}
					for (var i in question_frames)
					{
						func(question_frames[i].body, getBubblingEvents(events.question.list), attributes);//eventLogger stores dom events
					}
				}*/
        }
    };
    var bindEventsTracer = function()
    {
        var events = _model.GetEvent();
        var func;
        var attributes = null;

        if ((typeof(events.all) == 'undefined') && (typeof(events.item) != 'undefined') && (typeof(events.stimulus) != 'undefined') && (typeof(events.question) != 'undefined'))
        {
            for (var i in frameStored_array)
            {
                bind_platform(frameStored_array[i].body, getBubblingEvents({
                    type: ''
                }),{});//eventLogger stores dom events
            }
        /*for (var i in moduleStored_object)
				{
					bind_platform(moduleStored_object[i].body, getBubblingEvents({type: ''}),{});//eventLogger stores dom events
				}*/
        }
        else
        {
            if (typeof(events.all) != 'undefined')
            {
                attributes = {
                    attributes: events.all.attributes.split(','), 
                    factorize: events.all.factorize
                    };
                if (events.all.list.type == 'catch')
                {
                    func = bind_platform;
                }
                else
                {
                    func = unbind_platform;
                }
                for (var i in frameStored_array)
                {
                    func(frameStored_array[i].body, getBubblingEvents(events.all.list), attributes);//eventLogger stores dom events
                }
            /*for (var i in moduleStored_object)
					{
						func(moduleStored_object[i].body, getBubblingEvents(events.all.list), attributes);//eventLogger stores dom events
					}*/
            }
            else
            {
                for (var i in frameStored_array)
                {
                    bind_platform(frameStored_array[i].body, getBubblingEvents({
                        type:'', 
                        value:''
                    }), attributes);//eventLogger stores dom events
                }
            /*for (var i in moduleStored_object)
					{
						bind_platform(moduleStored_object[i].body, getBubblingEvents({type:'', value:''}), attributes);//eventLogger stores dom events
					}*/
            }
            if (typeof(events.item) != 'undefined')
            {
                attributes = {
                    attributes: events.item.attributes.split(','), 
                    factorize: events.item.factorize
                    };
                if (events.item.list.type == 'catch')
                {
                    func = bind_platform;
                }
                else
                {
                    func = unbind_platform;
                }
                for (var i in frameStored_array)
                {
                    func(frameStored_array[i].body, getBubblingEvents(events.item.list), attributes);//eventLogger stores dom events
                }
            /*for (var i in moduleStored_object)
					{
						func(moduleStored_object[i].body, getBubblingEvents(events.item.list), attributes);//eventLogger stores dom events
					}*/
            }
            if (typeof(events.stimulus) != 'undefined')
            {
                attributes = {
                    attributes: events.stimulus.attributes.split(','), 
                    factorize: events.stimulus.factorize
                    };
                if (events.stimulus.list.type == 'catch')
                {
                    func = bind_platform;
                }
                else
                {
                    func = unbind_platform;
                }
                for (var i in stimulus_frames)
                {
                    func(stimulus_frames[i].body, getBubblingEvents(events.stimulus.list), attributes);//eventLogger stores dom events
                }
            }
            if (typeof(events.question) != 'undefined')
            {
                attributes = {
                    attributes: events.question.attributes.split(','), 
                    factorize: events.question.factorize
                    };
                if (events.question.list.type == 'catch')
                {
                    func = bind_platform;
                }
                else
                {
                    func = unbind_platform;
                }
                for (var i in question_frames)
                {
                    func(question_frames[i].body, getBubblingEvents(events.question.list), attributes);//eventLogger stores dom events
                }
            }
        }
    };
    var triggerEvent = function(event, moduleId, params)
    {
        var pload = {};
        jQuery.extend(false, pload, params);//no deep copy to avoid endless objects (such as event objects)
        if (moduleId)
        {
            pload.moduleId = moduleId;
        }
        feedTrace('MODULE', event, new Date().getTime(), pload);

        if ((event == '_release_held_events') && (moduleId == 'platform'))
        {
            holdEvents = false;
            while (ev = heldEvents.shift())
            {
                triggerEvent(ev.event, ev.moduleId, ev.params);
            }
        }
        else
        {
            if (holdEvents)
            {
                heldEvents.push({
                    event:event, 
                    moduleId:moduleId, 
                    params:params
                });
            }
            else
            {
                for (var i in naxEvents[event])
                {
                	if (naxEvents[event][i].callback)
						try
						{
							//check context of execution
							naxEvents[event][i].callback.call( naxEvents[event][i].pageWindow, params, moduleId );
						}
						catch(e)
						{
							console.log('exception in callback for event ',event, ' from module ', moduleId, ' : ', e);
							delete  naxEvents[event][i];//might be not reachable anymore ()
						}
                }
            }
        }
    };
    var addCallbackEvent = function(event, callback, pageWindow)
    {
        if (typeof(naxEvents[event]) == 'undefined')
        {
            naxEvents[event] = new Array();
        }
        naxEvents[event].push({
            callback: callback, 
            pageWindow: pageWindow
        });
    };
    var removeCallback = function(event, callback)
    {
        if (typeof(callback) == 'undefined')
        {
            delete naxEvents[event];
        }
        else
        {
            for (var i in naxEvents[event])
            {
                if (naxEvents[event][i].callback == callback)
                {
                    delete naxEvents[event][i];
                }
            }
        }
    };
    var describeEvent = function(e,pload)
    {
        if (e.target && (typeof(e.target['value']) != 'undefined') && (e.target['value'] != -1) && (e.target['value'] != ''))
        {
            pload['value'] = $(e.target).value;
        }

        // get everything about the event
        for (var i in e)
        {
            if ((typeof(e[i]) != 'undefined') && ((typeof(e[i]) == 'string') | (typeof(e[i]) == 'number')) && (typeof(e[i]) != 'function') && (e[i] != ''))
            {
                if ((i != 'cancelable') && (i != 'contentEditable') && (i != 'bubbles') && (i != i.toUpperCase()) && (i != 'eventPhase') )// strings in toUpperCase are constants
                {
                    pload[i] = e[i];
                }
            }
        }
        return pload;
    };
    var describeElement = function(e,pload)
    {
        // take everything except useless attributes
        //namespaceURI, jquery version, html content (innerHTML and textContent)
        for (var i in e.target)
        {
            try
            {
                if ( ((typeof(e.target[i]) == 'string') | (typeof(e.target[i]) == 'number')) && (e.target[i] != '') && (i.substr(0,6) != 'jQuery') && (i != i.toUpperCase()) && (i != 'innerHTML') && (i != 'textContent') )
                {
                    pload[i] = e.target[i];
                }
            }
            catch(e){}
        }
        if (typeof(e.target.nodeName) != 'undefined')
        {
            switch(e.target.nodeName.toLowerCase())
            {
                case 'select':
                {
                    pload['value'] = $(e.target).val();
                    if (typeof(pload['value']) == 'array')
                    {
                        pload['value'] = pload['value'].join('|');
                    }
                    break;
                }
                case 'textarea':
                {
                    pload['value'] = $(e.target).val();
                    break;
                }
                case 'input':
                {
                    pload['value'] = $(e.target).val();
                    break;
                }
                case 'html':// case of iframe in design mode, equivalent of a textarea but with html
                {
                    if (e.target.ownerDocument.designMode == 'on')
                    {
                        pload['value'] = $(e.target).contents('body').html();
                    }
                    break;
                }
            }
        }
        return pload;
    };
    /*
		eventstation : callback to catch dom events
		*/
    var eventStation = function(e)
    {
        var target_tag = e.target.nodeName ? e.target.nodeName.toLowerCase():e.target.type;
        var idElement;

        if ((e.target.id) && (e.target.id.length > 0))
        {
            idElement = e.target.id;
        }
        else
        {
            idElement = 'noID';
        }
        if (e.type == 'keyup')
        {
            isShortcuts(e);//check is is a shortcut send appropriate piaacEvent
        }
        var pload = {
            'id' : idElement
        };
        if ((e.data != null) && (e.data.attributes != ''))//if attributes specified
        {
            for (var i in e.data.attributes)
            {
                if (e.data.attributes[i] == 'value')
                {
                    if (typeof(e.target.nodeName) != 'undefined')
                    {
                        switch(e.target.nodeName.toLowerCase())
                        {
                            case 'select':
                            {
                                pload['value'] = $(e.target).val();
                                if (typeof(pload['value']) == 'array')
                                {
                                    pload['value'] = pload['value'].join('|');
                                }
                                break;
                            }
                            case 'textarea':
                            {
                                pload['value'] = $(e.target).val();
                                break;
                            }
                            case 'input':
                            {
                                pload['value'] = $(e.target).val();
                                break;
                            }
                            case 'html':// case of iframe in design mode, equivalent of a textarea but with html
                            {
                                if (e.target.ownerDocument.designMode == 'on')
                                {
                                    pload['value'] = $(e.target).contents('body').html();
                                }
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (typeof(e[ e.data.attributes[i] ]) != 'undefined')
                    {
                        pload[ e.data.attributes[i] ] = e[ e.data.attributes[i] ];
                    }
                    else
                    {
                        if (typeof(e.target[ e.data.attributes[i] ]) != 'undefined')
                        {
                            pload[ e.data.attributes[i] ] = e.target[ e.data.attributes[i] ];
                        }
                    }
                }
            }
        }
        else//no attributes specified
        {
            pload = describeEvent(e,pload);//add events properties
            pload = describeElement(e,pload);//add dom element properties
        }
        if (e.data != null)
        {
            for (var i in e.data.factorize)
            {
                var val = '';
                var list = e.data.factorize[i].value.split(',');
                for (var j in  list)
                {
                    if (typeof(e[ list[i] ]) != 'undefined')
                    {
                        val = val + "," + e[ list[j] ];
                    }
                    else
                    {
                        if (typeof(e.target[ list[j] ]) != 'undefined')
                        {
                            val = val + "," + e.target[ list[j] ];
                        }
                    }
                }
                if (val != '')
                {
                    val = val.substring(1);//removing ',' at beginning of string
                    pload[ e.data.factorize[i].name ] = val;
                }
            }
        }
        feedTrace(target_tag, e.type, e.timeStamp, pload);
    };
    var feedTrace = function(target_tag, event_type, time, pload)
    {
        if (! pload)
        {
            pload = {};
        }
        pload.eventCounter = eventCounter;
        eventCounter++;

        var unitId =  _model.GetCurrentUnitID();
        pload.unitId = unitId;
        pload.itemName = _model.GetCurrentItemID();
        var itemId = _model.GetItemName();
        pload.itemId = itemId;//http://forge.tao.lu/issues/1753 : setting itemName as ItemId

        if (event_type == 'scoring')
        {
            var score = 0;
            try
            {
                if (typeof(pload.ictScore) != 'undefined')
                {
                    score = pload.ictScore;
                }
                else
                {
                    if (pload.result == 1)
                    {
                        score = 1;
                    }
                }
            }
            catch(e)
            {
            }
            try
            {
                var scoring = _model.GetContext('setAndFeedScore_unit'+unitId);
                if (scoring == null)
                {
                    scoring = {};
                }
                scoring[itemId] = {
                    'score': score, 
                    'pload': pload
                };

                _model.SetContext('setAndFeedScore_unit'+unitId, scoring, false);
            /*_model.SetScore(score);
					_model.FeedScore(pload);*/
            }
            catch(e)
            {}
        }
        var TaoAlive = false;//tao not there yet; TODO : add TAO and service isalive
        if (TaoAlive)
        {
        //staticModules.tao.window.eventTracer.feedTrace(target_tag, event_type, time, pload);//ajax request
        }
        storeEventToModel(target_tag, event_type, time, pload);
    };
    var eventsArray = new Array();
    var LocalStorage_Feedtrace_PacketSize = 499;
    var storeEventToModel = function(target_tag, event_type, time, pload)
    {
        if (! pload)
        {
            pload = {};
        }
        pload.target = target_tag;
        pload.event_name = event_type;
        pload.time = time;
        pload.lang = _model.GetLang();//lang of test

        eventsArray.push(pload);
        if ((eventsArray.length > LocalStorage_Feedtrace_PacketSize) | (event_type == 'custom') | (event_type == 'scoring'))
        {
            flush();
        }
    };
    var flush = function()
    {
        //sendAllFeedTrace();
	if (eventsArray.length > 0)
            _model.FeedTrace(eventsArray);//local storage
        delete eventsArray;
        eventsArray = new Array();
    };
    /*
		bind bubbling events and non bubbling events to eventstation
		+ action counter : counts the clicks and 
		*/
    var bind_platform = function(domElement, events, attributes)
    {
        //selection binded to window. must be in the list
        $(domElement).bind(events.bubbling.join(' '), attributes, eventStation);
        $(domElement).bindDom(events.nonBubbling.join(' '), attributes, eventStation);
    };
    /*
		unbind events from eventstation
		'attributes' parameter useless, only for call compatibility with bind_platform
		*/
    var unbind_platform = function(domElement, events, attributes)
    {
        if (jQuery.inArray('select', events.bubbling) != -1)
        {
            $(domElement.context.defaultView).unbind('select', eventStation);
        }
        $(domElement).unbind(events.bubbling.join(' '), eventStation);
        $(domElement).unBindDom(events.nonBubbling.join(' '), eventStation);
    };
    var sendAllFeedTrace = function()
    {
        staticModules.tao.window.eventTracer.sendAllFeedTrace_now();
    };
    var validationCallback_stimulus_array = new Array();
    var validationCallback_question_array = new Array();
    var setValidationCallback = function(callback, id)
    {
        if (id == 'stimulus')
        {
            validationCallback_stimulus_array.push(callback);
        }
        if (id == 'question')
        {
            validationCallback_question_array.push(callback);
        }
    };
    var validate = function()
    {
        for (var i in validationCallback_stimulus_array)
        {
            validationCallback_stimulus_array[i]();
        }
        for (var i in validationCallback_question_array)
        {
            validationCallback_question_array[i]();
        }
    };
    var removeValidation = function(id)
    {
        if (id == 'question')
        {
            delete validationCallback_question_array;
            validationCallback_question_array = new Array();
        }
        if (id == 'stimulus')
        {
            delete validationCallback_stimulus_array;
            validationCallback_stimulus_array = new Array();
        }
    };
		
    var setContext = function(key, value, is_global)
    {
        _model.SetContext(key, value, is_global);
    };
		
    var getContext = function(key, is_global)
    {
        return _model.GetContext(key, is_global);
    };
		
    var eventLoadItem = function(e)
    {
        switch(e.data.type)
        {
            case 'question':
            {
                questionLoaded = true;
                triggerEvent('QuestionLoaded', 'platform', {});
                break;
            }
            case 'stimulus':
            {
                stimulusLoaded = true;
                triggerEvent('stimulusLoaded', 'platform', {});
                break;
            }
            default:
            {
                console.log('eventloaditem error : default case reached');
            }
        }
        if (questionLoaded && stimulusLoaded)
        {
            triggerEvent('StimulusAndQuestionLoaded', 'platform', {});
        }
    };
		
    var ReinitializeActioncounter = function()
    {
        //actionsCount = 0;
        var unitId = _model.GetCurrentUnitID();
        var itemId = _model.GetItemName();
		var scoring = _model.GetContext('setAndFeedScore_unit'+unitId);
		if (scoring != null && scoring[itemId])
		{
			var itemData = scoring[itemId];
			actionsCount = itemData.pload.NbrAction;
			timeFirstAction = itemData.pload.TimeFAction;
		}
		else {
			actionsCount = 0;
			timeFirstAction = 0;
		}
        
    };
		
    var removeAllContextForProcess = function(processUri)
    {
        _model.RemoveAllContextForProcess(processUri);
    };

    var resetUnitForProcess = function(processUri)
    {
        _model.ResetUnitForProcess(processUri);
    };
		
    var getMeta = function()
    {
        return _model.GetMeta();
    };
    var setTimeFirstAction = function()
    {
        timeFirstAction = _model.GetGlobalTime().item;
    };
    var getClipboardValue = function()
    {
        var pasteText = '';
        netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');
        var clip = Components.classes["@mozilla.org/widget/clipboard;1"].createInstance(Components.interfaces.nsIClipboard);
        if (!clip) return false;
        var trans = Components.classes["@mozilla.org/widget/transferable;1"].createInstance(Components.interfaces.nsITransferable);
        if (!trans) return false;
        trans.addDataFlavor("text/unicode");
        clip.getData(trans,clip.kGlobalClipboard);
        var str = new Object();
        var strLength = new Object();
        try
        {
            trans.getTransferData("text/unicode",str,strLength);
            if (str) str = str.value.QueryInterface(Components.interfaces.nsISupportsString);
            if (str) pasteText = str.data.substring(0,strLength.value / 2);
        }
        catch(e)
        {
        //console.log(e);
        }
        return pasteText;
    };
    var setClipboardValue = function(text)
    {
        try
        {
            netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');
            var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
            str.data=text;
            var trans = Components.classes["@mozilla.org/widget/transferable;1"].createInstance(Components.interfaces.nsITransferable);
            trans.addDataFlavor("text/unicode");
            trans.setTransferData("text/unicode",str,text.length * 2);
            var clipid = Components.interfaces.nsIClipboard;
            var clip = Components.classes["@mozilla.org/widget/clipboard;1"].getService(clipid);
            clip.setData(trans,null,clipid.kGlobalClipboard);
        }
        catch(e)
        {
            const gClipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"].  
            getService(Components.interfaces.nsIClipboardHelper);  
            gClipboardHelper.copyString(text);
        }
    };
    var copy = function(text)
    {
        setClipboardValue(text);
			
        var PLoad = {
            content: text
        };
        feedTrace(focusedField.nodeName, 'COPY', new Date().getTime(), PLoad);
    };
    var getSelectedText = function()
    {
        return selectedText;
    };
    var setSelectedtext = function(text)
    {
        if (text != null)
        {
            selectedText = text;
        }
    };
    var paste = function()
    {
        if ((typeof(focusedField) == 'undefined') || (focusedField == null))
        {
            return 0;
        }
        // empty element, just add the text
        var textToPaste = getClipboardValue();
        switch(focusedField.nodeName.toLowerCase())
        {
            case 'input':
            case 'textarea':
            {
                if ( $(focusedField).val().length < 1 )
                {// if empty
                    $(focusedField).val(textToPaste);
                }
                else
                {// else check for selection to replace
                    var text_begin = $(focusedField).val().substring(0,focusedField.selectionStart);
                    var text_end = $(focusedField).val().substring(focusedField.selectionEnd);
                    var new_text = text_begin + textToPaste + text_end;
                    $(focusedField).val(new_text);
                }
                triggerEvent('Paste', 'Platform', {
                    targetElement: focusedField
                });
                break;
            }
            case 'body'://editable iframe (designMode = on)
            {
                var textContent = $.trim($(focusedField).text());// remove white characters
                if (textContent == '')
                {
                    $(focusedField).text(textToPaste);
                }
                else
                {
                    var focused_doc = focusedField.ownerDocument;
                    var sel = focused_doc.defaultView.getSelection();
                    var range = sel.getRangeAt(0);
                    range.deleteContents();//selection is deleted
                    range.insertNode(focused_doc.createTextNode(textToPaste));
                }
                break;
            }
        }
        $(focusedField).focus();
        var PLoad = {
            content: textToPaste
        };
        feedTrace(focusedField.nodeName, 'PASTE', new Date().getTime(), PLoad);
    };
    var cut = function()
    {
			
    };
    var isShortcuts = function(event)
    {
        var keys = {
            alt:{}, 
            shift:{}, 
            key:event.which
            };
        if (event.altKey)
        {
            keys.alt[event.which] = 1;
        }
        if (event.shiftKey)
        {
            keys.shift[event.which] = 1;
        }
        triggerEvent('shortcut', 'shortcut', keys);
    };
    return {
        setHoldEvents: function()
        {
            holdEvents = true;
        },
        SetSelectedText: function(text)
        {
            setSelectedtext(text);
        },
        GetSelectedText: function()
        {
            return getSelectedText();
        },
        copyEventCatched: function(e)
        {
            var PLoad = {};
            var name = '';
            if (focusedField != null)
            {
                name = focusedField.nodeName;
            }
            feedTrace(name, 'COPY', new Date().getTime(), PLoad);
        },
        pasteEventCatched: function(e)
        {
            var PLoad = {};
            var name = '';
            if (focusedField != null)
            {
                name = focusedField.nodeName;
            }
            feedTrace(name, 'PASTE', new Date().getTime(), PLoad);
            triggerEvent('Paste', 'Platform', {});
        },
        cutEventCatched: function(e)
        {
            var PLoad = {};
            var name = '';
            if (focusedField != null)
            {
                name = focusedField.nodeName;
            }
            feedTrace(name, 'CUT', new Date().getTime(), PLoad);
        },
        trigger_copy: function()
        {
            copy(selectedText);
        },
        trigger_paste: function()
        {
            paste();
        },
        trigger_cut: function()
        {
            cut();
        },
        setFocusedField: function(e)
        {
            focusedField = e.target;
        },
        /*public functions (API)*/
        Start: function()
        {
            ShowModulesUnit();
            ShowItem();
        },
        /*naxApi functions*/
        GetCountActions: function()
        {
            return getBubblingEvents({
                value: countedActions, 
                type:'actionCounter'
            });
        },
        GetActionsCount: function()
        {
            return actionsCount;
        },
        ActionCounter: function(e)
        {
            if (actionsCount < 1)
            {
                setTimeFirstAction();
            }
            actionsCount++;
        },
        GetTimeFirstAction: function()
        {
            return timeFirstAction;
        },
        StoreNewModule: function(id, window, body)
        {
            //find moduleParams
            var moduleParams = null;
            var context = {
                context: null, 
                container: null
            };
				
            switch(id)
            {
                case 'stimulus':
                {
                    StimulusQuestionModules.stimulus.window = window;
                    break;
                }
                case 'question':
                {
                    StimulusQuestionModules.question.window = window;
                    break;
                }
                case 'tao':
                {
                    staticModules.tao.window = window;
                    break;
                }
                case 'multihighlight':
                {
                    staticModules.multihighlight.window = window;
                    break;
                }
            }

            if (typeof(unitModulesInstancied[id]) != 'undefined')
            {
                moduleParams = unitModulesInstancied[id].module.params;
                unitModulesInstancied[id].module.window = window;
                context.context = 'unit';
                context.container = unitModulesInstancied[id].frame;
            }
            else
            {
                if (typeof(itemModulesInstancied[id]) != 'undefined' )
                {
                    moduleParams = itemModulesInstancied[id].module.params;
                    itemModulesInstancied[id].module.window = window;
                    context.context = 'item';
                    context.container = itemModulesInstancied[id].frame;
                }
            }
            moduleStored_object[id] = {
                window: window, 
                moduleParams: moduleParams, 
                context: context, 
                body: body
            };
			//console.log("nax: moduleStored_object " + id);	//DEBUG
           testFramesLoaded();
        },
        StoreNewFrame: function(id, window, body)//called in naxApi.js at page load
        {
            frameStored_array.push({
                window: window, 
                body: body
            });
            switch(id)
            {
                case 'question':
                {
                    $(window).bind('load', {
                        type:'question'
                    }, eventLoadItem);
                    question_frames.push({
                        window: window, 
                        body: body
                    });
                    break;
                }
                case 'stimulus':
                {
                    $(window).bind('load', {
                        type:'stimulus'
                    }, eventLoadItem);
                    stimulus_frames.push({
                        window: window, 
                        body: body
                    });
                    break;
                }
                default:
                {
                    bindEventsTracer_module(body);//applying feedtrace to unknown elements (stimulus events)
                }
            }

            testFramesLoaded();
        },
        GetNaxModuleParams: function(id)
        {
            return moduleStored_object[id].moduleParams;
        },
        GetNaxContext: function(id)
        {
            if (typeof(moduleStored_object[id]) != 'undefined')
            {
                return moduleStored_object[id].context;
            }
            else
            {
                return null;
            }
        },
        IncrementFrameStored: function(x)//counts the frames found in modules
        {
            framesCount = framesCount + x;
        },
        GetBaseUrl: function()
        {
            return BASE_URL;
        },
        GetModuleUrl: function()
        {
            return BASE_URL+config['SUFFIX_MODULE'];
        },
        GetUnitUrl: function()
        {
            return BASE_URL+config['SUFFIX_UNIT'];
        },
        GetServiceUrl: function()
        {
            return BASE_URL+config['SUFFIX_SERVICE'];
        },
        GetProcessUri:function()
        {
            return _model.GetProcessUri();
        },
        GetLang: function()
        {
            return  _model.GetLang();
        },
        GetUnitNumber: function()
        {
            return _model.GetUnitNumber();
        },
        GetItemNumber: function()
        {
            return _model.GetItemNumber();
        },
        GetTotalItemNumber: function()
        {
            return _model.GetTotalItemNumber();
        },
        Util: function()
        {
            return util;
        },
        GetGlobalTime: function()
        {
            return _model.GetGlobalTime();
        },
        GetMap: function()
        {
            return _model.GetMap();
        },
        GetSectionMap: function()
        {
            return _model.GetSectionMap();
        },
        GetMeta: function()
        {
            return getMeta();
        },
        GetCurrentUnitID: function()
        {
            return _model.GetCurrentUnitID();
        },
        GetCurrentItemID: function()
        {
            return _model.GetCurrentItemID();
        },
        GetCurrentItemName: function()
        {
            return _model.GetItemName();
        },
        GetFormID: function()
        {
        	return _model.GetFormID();
        },
        GetRule: function()
        {
            return _model.GetRule();
        },
        GetScoringAttributes: function()
        {
            return _model.GetScoringAttributes();
        },
        Feedtrace: function(target_tag, event_type, time, pLoad)
        {
            if (! pLoad)
            {
                pLoad = {};
            }
            feedTrace(target_tag, event_type, time, pLoad);
        },
        Next: function()
        {
            /*
					triggers event 'onItemEnd', callback in platform
				*/
            if (platformReady)//prevent multiple calls when loading
            {
                validate();
                triggerEvent('onItemEnd', 'controler', {});
                flush();
/*END ITEM ACTIONS*/
                if (_model.GetItemNumber() >= _model.GetTotalItemNumber())
                {
                    //if last item of unit, sending scoring info
                    var scoring = _model.GetContext('setAndFeedScore_unit'+_model.GetCurrentUnitID());
                    for (var i in scoring)
                    {
                            _model.SetScore(scoring[i].score);
                            _model.FeedScore(scoring[i].pload);
                    }
                }
                delete frameStored_array;
                frameStored_array = new Array();
                delete naxEvents['scoreNowResult'];
                delete naxEvents['scoreNowEvent'];//TODO : test
                delete naxEvents['PSReadyToScore'];
                delete naxEvents['PSscored'];
                if(_model.PerformNextItemTransition())
                    ShowItem();
            }
        },
        Previous: function()
        {
            if (platformReady)//prevent multiple calls when loading
            {
                validate();
                triggerEvent('onItemEnd', 'controler', {});
                flush();
                delete frameStored_array;
                frameStored_array = new Array();
                delete naxEvents['scoreNowResult'];
                delete naxEvents['scoreNowEvent'];//TODO : test
                delete naxEvents['PSReadyToScore'];
                delete naxEvents['PSscored'];
                if(_model.PerformPreviousItemTransition())
                    ShowItem();
            }
        },
        Jump: function(itemId)
        {
            if (platformReady)//prevent multiple calls when loading
            {
                validate();
                triggerEvent('onItemEnd', 'controler', {});
                flush();
                delete frameStored_array;
                frameStored_array = new Array();
                delete naxEvents['scoreNowResult'];
                delete naxEvents['scoreNowEvent'];//TODO : test
                delete naxEvents['PSReadyToScore'];
                delete naxEvents['PSscored'];
                if(_model.PerformJumpItemTransition(itemId))
                    ShowItem();
            }
        },
        Breakoff: function()
        {
            if (platformReady)//prevent multiple calls when loading
            {
                flush();
                delete frameStored_array;
                frameStored_array = new Array();
                delete naxEvents['scoreNowResult'];
                delete naxEvents['PSReadyToScore'];
                delete naxEvents['PSscored'];
                _model.PerformBreakOffTransition();
            }
        },
        SetValidationCallback: function(callback, id)
        {
            setValidationCallback(callback, id);
        },
        RemoveValidation: function(id)
        {
            removeValidation(id);
        },
        AddCallbackEvent: function(event, callback, pageWindow)
        {
            addCallbackEvent(event, callback, pageWindow);
        },
        TriggerEvent: function(event, moduleId, params)
        {
            triggerEvent(event, moduleId, params);
        },
        RemoveCallback: function(event, callback)
        {
            removeCallback(event, callback);
        },
        LoadModule: function(moduleName, moduleId, context, params, callback, container)
        {
            var module = loadModule({
                module: moduleName, 
                id: moduleId, 
                params: params
            }, context, callback, container);
            return module;
        },
        LoadModuleInLang: function(moduleName, moduleId, lang, context, params, callback, container)
        {
            var module = loadModule({
                'module': moduleName, 
                'id': moduleId, 
                'lang': lang, 
                'params': params
            }, context, callback, container);
            return module;
        },
        UnLoadModule: function(module)
        {
            unLoadModule(module);
        },
        IsModuleLoaded: function(id, context)
        {
            return isModuleLoaded(id, context);
        },
        GetSelectedText: function()
        {
            return getSelectedText();
        },
        SetSelectedtext: function(text)
        {
            setSelectedtext(text);
        },
        IsPlatformReady: function()
        {
        	return isPlatformReady();
        },
        /*tao api*/
        setUserVar: function(key, value, isGlobal)
        {
        	if (isGlobal)
            	setContext(key, value, 'true');
			else        		
            	setContext(key, value, 'false');
        //staticModules.tao.window.setUserVar(key, value);//tao
        },
        getUserVar: function(key, isGlobal)
        {
        	if (isGlobal)
				return getContext(key, 'true');
			else
				return getContext(key, 'false');
        },
        removeAllContextForProcess: function()
        {
            removeAllContextForProcess(_model.GetProcessUri());
        },
        resetUnitForProcess: function()
        {
            resetUnitForProcess(_model.GetProcessUri());
        },
        getEndorsment: function()
        {
            return this.staticModules.tao.window.getEndorsment();
        },
        setEndorsment: function(endorsment)
        {
            this.staticModules.tao.window.setEndorsment(endorsment);
        },
        getScore: function()
        {
            return this.staticModules.tao.window.getScore();
        },
        setScore: function(score)
        {
            this.staticModules.tao.window.setScore(score);
        },
        getScoreRange: function()
        {
            return this.staticModules.tao.window.getScoreRange();
        },
        setScoreRange: function(max, min)
        {
            this.staticModules.tao.window.setScoreRange(max, min);
        },
        getAnsweredValues: function()
        {
            return this.staticModules.tao.window.getAnsweredValues();
        },
        setAnsweredValues: function(values)
        {
            this.staticModules.tao.window.setAnsweredValues(values);
        },
        getSubject: function()
        {
            return this.staticModules.tao.window.getSubject();
        },
        getSubjectLogin: function()
        {
            return this.staticModules.tao.window.getSubjectLogin();
        },
        getSubjectName: function()
        {
            return this.staticModules.tao.window.getSubjectName();
        },
        getItem: function()
        {
            return this.staticModules.tao.window.getItem();
        },
        getTest: function()
        {
            return this.staticModules.tao.window.getTest();
        },
        getDelivery: function()
        {
            return this.staticModules.tao.window.getDelivery();
        }
    };
};
return {
    getInstance : function(model_){
        if(_instance == null){
            _instance = NaxControl(model_);
        }
        return _instance;
    }
};
})();


