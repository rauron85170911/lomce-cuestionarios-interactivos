var ItemApi = {
    _item: {
        'file': null, 
        'reload': true
    },
    reloadItem: function(file_, reload_, _loadCallback)
	{
		$('#item').load(_loadCallback);//maybe unload first
		var url;
		if (file_.indexOf('/') == 0)
			url = file_;
		else
			url = '../../.' + file_;
        if(reload_)
		{
            if(file_ != null)
			{
            	/* Avoid some page load messiness */
				$('#item').css('visibility', 'hidden');
				$('#item').load(function() { $(this).css('visibility', 'visible'); });
                //$('#item').attr('src', url);
                //$('#item').attr('src', url + '?ModuleId=' + __id + '&d=' + new Date().getTime());
                $('#item').attr('src', url + '?ModuleId=' + __id);
			}
        }
        else
		{
            if(file_ != this._item.file)
			{
            	/* Avoid some page load messiness */
				$('#item').css('visibility', 'hidden');
				$('#item').load(function() { $(this).css('visibility', 'visible'); });
                $('#item').attr('src', url + '?ModuleId=' + __id);
                //$('#item').attr('src', url + '?ModuleId=' + __id + '&d=' + new Date().getTime());
                //$('#item').attr('src', url);
			}
        }
        this._item.file = file_;
        this._item.reload = reload_;
		return $('#item')
    }
};
