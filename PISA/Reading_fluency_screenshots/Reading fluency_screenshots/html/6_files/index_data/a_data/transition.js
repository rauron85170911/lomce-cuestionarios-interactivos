$(window).load(function(){
	AddCallbackEvent('onItemBegin', function(){
		naxContext.container.css('display', 'none');
	});
	
	//Add a wait loop to check if platform is ready. Event isn't always getting fired it seems
	var checkCount = 0;
	var intervalID = setInterval(function(){
		if (IsPlatformReady() || checkCount++ > 25) {
			naxContext.container.css('display', 'none');
			clearInterval(intervalID);
		}
	}, 200);
	
    AddCallbackEvent('onItemEnd', function(){
        naxContext.container.css('display', 'block');
    });
});
