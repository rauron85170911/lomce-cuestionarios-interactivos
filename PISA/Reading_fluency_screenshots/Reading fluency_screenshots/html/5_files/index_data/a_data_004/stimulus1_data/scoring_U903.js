/* globalize scoring variables that must be used during the test and the variables that have to be gathered for the final scoring method */

$(window).load(function()
{
//to start feeding the scoring object (you will then find in the traces)
	AddCallbackEvent('PSReadyToScore', score_U903);	
});

// scoring method called after the next arrow has been pressed.
function score_U903()
{
	/* scored variables */
	var sentenceData = getUserVar("SentenceData_" + getCurrentUnitID() + "_" + getCurrentItemID());
	var totalScore = 0;
	var bitScore = 0;
	var pow = 0;
	
	// add variables output and outcome to the final scoring thatis returned.
	for (var i in sentenceData) {
		var data = sentenceData[i];
		addVarToScoringObject("Unit902" + data.itemID + '_selected', data.selected);
		addVarToScoringObject("Unit902" + data.itemID + '_time', data.time);
		addVarToScoringObject("Unit902" + data.itemID + '_score', (data.correct ? "1" : "7"));
		if (data.correct) {
			totalScore += 1;
			bitScore += Math.pow(2, pow);
		}
		pow++
	}
	addVarToScoringObject('result', totalScore);
	addVarToScoringObject('bitScore', bitScore);
	TriggerEvent('PSscored', 'scoring', {"unitNumber":"Unit903_"});
	
}

