// always use this name
var js_translation = new Object();

js_translation['title'] = 'Confirmaci&#xF3;n';
js_translation['message'] = 'Vas a salir de esta unidad y NO podr&#xE1;s modificar estas respuestas.<br/><br/>&#xBF;Seguro que quieres ir a la siguiente unidad?';
js_translation['qConfirm'] = 'XXX';
js_translation['ok'] = 'S&#xCD;';
js_translation['cancel'] = 'NO';
js_translation['calculator'] = 'Calculadora';
