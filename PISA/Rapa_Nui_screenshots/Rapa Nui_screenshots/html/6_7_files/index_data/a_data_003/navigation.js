var __navDisabled = false;
var __prevDisabled = false;
var __navigationCallbacks = [];

$(window).load(function(){
	if (getLang() == 'qar-ARE') {
        var unitID = getCurrentUnitID(); 
		if (unitID && ((unitID.substr(0,3) == 'MAT') || (unitID.substr(0,3) == 'SCI') || (unitID.substr(0,3) == 'XYZ' && (unitID.indexOf('Math') != -1 || unitID.indexOf('Science') != -1 )))) {
			$.ajax({
				type: "GET",
				url: getUnitUrl() + "/../module/navigation/eng-ARE/js_translation.js",
				dataType: "text",
				async: false,
				success: function(txt) {
					var jsKeys = ['Title', 'message', 'ok', 'cancel', 'calculator'];
					for (var i in jsKeys) {
						var RE = new RegExp("(js_translation\\['" + jsKeys[i] + "'\\])(\\s*)(={1})(\\s*)('(?:[^'\\\\]|\\\\.)*')","gi");
						var result = txt.match(RE);
						if(result != null){
							eval(result[0]); // add to js_tranlation via eval
						}
					}
				}
			});
        }
	}
    initializeNavigation();
    
    __naxControler.disableNavigation = disableNavigation;
    __naxControler.enableNavigation = enableNavigation;
    __naxControler.disablePrev = disablePrev;
	__naxControler.enablePrev = enablePrev;
	__naxControler.setNavigationCallback = setNavigationCallback;
	__naxControler.removeNavigationCallback = removeNavigationCallback;
    __naxControler.navigation = window;
});

function disableNavigation() {
	__navDisabled = true;
    $('#previous, #next').unbind('click');
    $('#previous, #next').attr('disabled', 'disabled');
}

function enableNavigation() {
	__navDisabled = false;
	initializeNavigation();
}

function enablePrev() {
	__prevDisabled = false;
	initializeNavigation();
}

function disablePrev() {
	__prevDisabled = true;
    $('#previous').unbind('click');
    $('#previous').attr('disabled', 'disabled');
}

function hideNavigation() {
    $('#previous, #next, #help').css('visiblity', 'hidden');
}

function showNavigation() {
    $('#previous, #next, #help').css('visiblity', 'visible');
}

function showCalculator() {
	var calcDialog = window.parent.$("#calcDialog");
	if (calcDialog.length == 0) {
		calcDialog = window.parent.$("<div></div>");
		calcDialog.attr('id', 'calcDialog');
		window.parent.$('#naxContainer').append(calcDialog);
		calcDialog.dialog({
				autoOpen: false,
				modal: false,
				resizable: false,
				width: 'auto',
				iframeFix: true,
				position: [975, 115],
				title: js_translation['calculator']
			});
	}
	
	var mod = LoadModule('calculator', 'calculator', 'item', {}, function(module){
		module.CalcApi.setCloseCallback(function(){
			calcDialog.dialog('close');
			UnloadModule(module);
		});
	}, calcDialog);
	
	//Try to avoid empty calc popup. Try in a few seconds to make sure it is loaded.
	window.setTimeout(function() {
		if (calcDialog.children().length == 0) {	//no calc found. try again to load
			mod = LoadModule('calculator', 'calculator', 'item', {}, function(module){
				module.CalcApi.setCloseCallback(function(){
					calcDialog.dialog('close');
					UnloadModule(module);
				});
			}, calcDialog);
		}
	}, 3000);
	
	
	$('#calculator').show();
	$('#calculator').unbind('click');
	$('#calculator').bind('click', function(){
		var calcDialog = window.parent.$("#calcDialog");
		calcDialog.dialog('open');
		if (calcDialog.length == 0) {
			//calcDialog.dialog({draggable: false}).parent().draggable();
		}
	});
}

function hideCalculator() {
	$('#calculator').unbind('click');
	$('#calculator').hide();
}

function initializeNavigation() {
	if (__navDisabled)
		return;
		
	var isFirstItem = isFirstItemOfCurrentUnit();
	var isLastItem = isLastItemOfCurrentUnit();
	
	$('#help').unbind('click');
	$('#help').bind('click', function(){
		var mod = LoadModule('help', 'help', 'item', {}, function(module){
			module.HelpApi.setCloseCallback(function(){
				UnloadModule(module);
			});
		});
	});
	
	var metas = getMeta();
	if (metas['type'] && (metas['type'] == "math" || metas['type'] == "financial" || metas['type'] == "xyz-math")) {
		showCalculator();
	}
	if (metas['type'] && (metas['type'].indexOf("xyz") != -1)) {
		$('#help').unbind('click');
		$('#help').attr('disabled', 'disabled');
	}
	
	$('#previous, #next').unbind('click');
	$('#previous, #next').removeAttr('disabled');
	$('#previous, #next').bind('click', function(){
		var idButton = $(this).attr('id');
		if (validateNavigation()) {
			feedtrace('Navigation', 'cancel', {direction: idButton});
			evt.stopPropagation();
			return false;
		}
		if((isFirstItem && idButton == 'previous')
			|| (isLastItem && idButton == 'next')) {
			var mod = LoadModule('dialog', 'dialog', 'item', {}, function(module){
				var ok = js_translation['ok'];
				var cancel = js_translation['cancel'];
				var buttons = {};
				buttons[$('<span>').html(ok).html()] = function() {
					module.DialogApi.closeDialog();
					if(idButton == 'next'){
						forward();
						initialize();
					}
					else if(idButton == 'previous'){
						backward();
						initialize();
					}
				};
				buttons[$('<span>').html(cancel).html()] = function() {
					module.DialogApi.closeDialog();
				};
				module.DialogApi.setButtons(buttons);
				module.DialogApi.setCloseCallback(function(){
					UnloadModule(module);
				});
				module.DialogApi.setResizable(false);
				module.DialogApi.setModal(true);
				module.DialogApi.setTitle(js_translation['title']);
				module.DialogApi.setMessage(js_translation['message']);
				module.DialogApi.showDialog();
			});
		}
		else {
			if(idButton == 'next'){
				forward();
				initializeNavigation();
			}
			else if(idButton == 'previous'){
				backward();
				initializeNavigation();
			}
		}
	});

	if(isFirstItem == true || __prevDisabled){
		$('#previous').unbind('click');
		$('#previous').attr('disabled', 'disabled');
	}
	else
		$('#previous').removeAttr('disabled');
}

function isFirstItemOfCurrentUnit(){
	var map = getMap();
	var currentUnitID = getCurrentUnitID();
	var currentItemID = getCurrentItemID();  
	var isFirstItemOfCurrentUnit = false;
	$.each(map, function(k1, v1){
		// Special case: item "0" cannot be returned to
		if(k1 == currentUnitID && ((v1.items[0] && v1.items[0] == currentItemID) || v1.items[1] == currentItemID)) 
			isFirstItemOfCurrentUnit = true;
	});
	return isFirstItemOfCurrentUnit;
}

function isLastItemOfCurrentUnit(){
	var map = getMap();
	var currentUnitID = getCurrentUnitID();
	var currentItemID = getCurrentItemID();  
	var isLastItemOfCurrentUnit = false;
	$.each(map, function(k1, v1){
		var itemCount = util.objectSize(v1.items);
		if(k1 == currentUnitID && v1.items[itemCount] == currentItemID) 
			isLastItemOfCurrentUnit = true;
	});
	return isLastItemOfCurrentUnit;
}

function getNavAttr(map) {
	var navAttr = '';
	var currentUnitID = getCurrentUnitID();
	var currentItemID = getCurrentItemID();  
	$.each(map, function(k1, v1){
		// Special case: item "0" cannot be returned to
		if(k1 == currentUnitID) {
			for (var i in v1.items) {
				if (v1.items[i].id == currentItemID && v1.items[i].navigation)
					navAttr = v1.items[i].navigation;
			}
		}
	});
	return navAttr;
}

function getWarningAttr(map) {
	var warningAttr = '';
	var currentUnitID = getCurrentUnitID();
	var currentItemID = getCurrentItemID();  
	$.each(map, function(k1, v1){
		// Special case: item "0" cannot be returned to
		if(k1 == currentUnitID) {
			for (var i in v1.items) {
				if (v1.items[i].id == currentItemID && v1.items[i].warning)
					warningAttr = v1.items[i].warning;
			}
		}
	});
	return warningAttr;
}

function hideTool(toolname) {
	$("#"+toolname).css('display', 'none');
}

function showTool(toolname) {
	$("#"+toolname).css('display', 'inline');
}

var setNavigationCallback = function(callback, id)
{
	__navigationCallbacks.push(callback);
};
var validateNavigation = function()
{
	for (var i in __navigationCallbacks)
	{
		if (__navigationCallbacks[i]())
			return true;
	}
	return false;
};
var removeNavigationCallback = function(id)
{
	delete __navigationCallbacks;
	__navigationCallbacks = new Array();
};
