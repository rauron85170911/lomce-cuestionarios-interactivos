var util = {    
    _cookies: {},
    
    getUrlVars : function()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    
    setCookie: function(name,value,days){
        //        var expires = '';
        //        if (days) {
        //            var date = new Date();
        //            date.setTime(date.getTime()+(days*24*60*60*1000));
        //            var expires = '; expires='+date.toGMTString();
        //        }
        //        document.cookie = name+'='+value+expires+'; path=/';
        
        this._cookies[name] = value;

        $.ajax({
            url: config['PATH_SERVICE'] + '/common/setcookie.php',
            type: 'POST',
            async: true,
            cache : false,
            data: 'NAME='+encodeURIComponent($.toJSON(name))+
            '&VALUE='+encodeURIComponent($.toJSON(value)),
            dataType: 'json'
        });
    },
    
    getCookie: function(name) {
        //        var nameEQ = name + '=';
        //        var ca = document.cookie.split(';');
        //        for(var i=0;i < ca.length;i++) {
        //            var c = ca[i];
        //            while (c.charAt(0)==' ') c = c.substring(1,c.length);
        //            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        //        }
        //        return null;
    
        if (typeof this._cookies[name] !== 'undefined')
            return this._cookies[name];
        else {
            var cookie = null;
            $.ajax({
                url: config['PATH_SERVICE'] + '/common/getcookie.php',
                type: 'POST',
                async: false,
                cache : false,
                data: 'NAME='+encodeURIComponent($.toJSON(name)),
                dataType: 'json',
                success: function(cookie_){
                    cookie = cookie_;
                }
            });
            return cookie;
        }
    },
    
    objectToArrayByKey: function(object) {
        if(typeof object !== 'object')
            return object;
        
        var array = [];
        for(var elem in object) {
            array[elem] = [];
            array[elem] = util.objectToArrayByKey(eval('object.' + elem));
        }
        return array;
    },
    
    objectToArrayByIndex: function(object) {
        if(typeof object !== 'object')
            return object;
        
        var array = [];
        for(var elem in object) {
            array.push(util.objectToArrayByIndex(eval('object.' + elem)));
        }
        return array;
    },
	
    replaceElementInArray: function(array, oldObj, newObj)
    {
        if (oldObj.jquery)
        {
            oldObj = oldObj[0];
        }
        var found = false;
        var i = 0;
        var arrayLength = array.length;
        while ((! found) && (i<arrayLength))
        {
            var item = array[i];
            if (item.jquery)
            {
                item = item[0];
            }
            if (item == oldObj)
            {
                found = true;
                array[i] = newObj;
            }
            else
            {
                i++;
            }
        }
		
        return array;
    },
    
    objectSize: function(obj)
    {
        var size = 0;
        for (var i in obj)
        {
            size++;
        }
        return size;
    }
};

/**
 * @description bind every non bubbling events to dom elements.
 * @methodOf added to jquery
 */
jQuery.fn.bindDom = function(events, attributes, callback)
{
    if (events != '')
    {
        $(this).bind(events, attributes, callback);
        var childrens = $(this).children();
        if (childrens.length)// stop condition
        {
            childrens.bindDom(events, attributes, callback);
        }
    }
};

/**
 * @description unbind platform events
 * @methodOf added to jquery
 */
jQuery.fn.unBindDom = function(events , callback)
{
    if (events != '')
    {
        $(this).unbind(events, callback);
        var childrens = $(this).children();
        if (childrens.length)// stop condition
        {
            childrens.unBindDom(events, callback);
        }
    }
};
