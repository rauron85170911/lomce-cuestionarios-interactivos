$(window).load(function(){
    var url_vars = util.getUrlVars();
    
    if ((navigator.userAgent.indexOf('Firefox') == -1 && navigator.userAgent.indexOf('Chrome') == -1)&& window.location.href.toLocaleLowerCase().indexOf("diagnostics") == -1) { 
    	alert("Your browser is currently not supported. Previews require Firefox or Chrome to function correctly."); 
    }
    
	var __theConfig = (url_vars['config'] != undefined ? url_vars['config'] : null);
    
    url_vars_controler = url_vars;
    if(url_vars['user'] != undefined
        && url_vars['unit'] != undefined 
        && url_vars['lang'] != undefined){
        if (url_vars['user'] == '')
            url_vars['user'] = (new Date()).valueOf();
        
        var NaxModelInstance;
        if(url_vars['domain'] == undefined)
            NaxModelInstance = NaxModel.getInstance(url_vars['user'], url_vars['unit'], url_vars['lang'], __theConfig);  
        else
            NaxModelInstance = NaxModel.getInstance(url_vars['user'], url_vars['domain'] + '/' + url_vars['unit'], url_vars['lang'], __theConfig);  
        var NaxControlInstance = NaxControl.getInstance(NaxModelInstance);
        if (url_vars['holdEvents'] != undefined)
        {
            url_vars['holdEvents'] = (url_vars_controler['holdEvents'] == 'true');
            NaxControlInstance.setHoldEvents();
        }
        NaxControlInstance.Start();
    }
});

var url_vars_controler = null;
