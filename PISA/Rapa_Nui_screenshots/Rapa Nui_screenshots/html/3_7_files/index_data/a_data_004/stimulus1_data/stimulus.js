$(window).load(function()
    {
    	$('.pagingUI').addClass('pagingUI-load');
    	
    	if ($('#pagingUIRight').length == 0)
    		setUserVar(getCurrentUnitID() + "_lastPageSeen", "1");
    	
    	var lastPageSeen = getUserVar(getCurrentUnitID() + "_lastPageSeen");
    	
    	if (!lastPageSeen) 
    		getControler().disableNavigation();
    	else
    		getControler().enableNavigation();
	}
);