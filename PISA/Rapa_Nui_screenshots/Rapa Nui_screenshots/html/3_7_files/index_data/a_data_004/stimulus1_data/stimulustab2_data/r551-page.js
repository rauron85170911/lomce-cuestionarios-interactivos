var tabURLs = {
	'blogLink': 'stimulustab2.html',
	'newsLink': 'stimulustab3.html'
};

$(document).ready(function(e) {
	$('.tabLink').click(function() {
		var tabID = $(this).attr('id');
		parent.openTab(tabID, js_translation[tabID + "Title"], tabURLs[tabID], true);
		// store the current URL
		var current_url = window.location.href;

		// use replaceState to push a new entry into the browser's history
		history.replaceState({},"",$(this).attr("href"));

		// use replaceState again to reset the URL
		history.replaceState({},"",current_url);
		e.preventDefault();
		e.stopPropagation();
		return false;
	});
	$('.tabLink').each(function(idx) {
		$(this).attr('href', '#' + parent.getProcessUri() + '-' + idx);
	});
	$('.tabLinkTarget').each(function(idx) {
		$(this).attr('name', '#' + parent.getProcessUri() + '-' + idx);
	});
});
