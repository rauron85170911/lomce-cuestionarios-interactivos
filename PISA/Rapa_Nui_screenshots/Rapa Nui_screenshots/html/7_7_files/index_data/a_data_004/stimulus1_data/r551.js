var tabs = null;
var openTabs = {};
var tabURLs = [];
var numTabs = 1;  //Start with one tab open

$(document).ready(function() {
	tabs = $( ".tabs" ).tabs({
		activate: function(ev,ui) {
			tabIndex = ui.newTab.index();
			setUserVar("R551_Tabs_Active", tabIndex);
		}
	});

	var userVar = getUserVar("R551_Tabs");
	var activeTab = getUserVar("R551_Tabs_Active");
	if (userVar) {
		$.each(userVar, function(k,v) {
			openTab(v.id, v.label, v.url, false);
		});
	}

	var itemName = getCurrentItemID();
	switch (itemName) {
		case 'item1': 
		case 'item2': 
		case 'item3': 
		case 'item4': 
		{
			activeTab = 0;
			break;
		}
		case 'item5': 
		case 'item6': 
		{
			if (!openTabs['blogLink']) 
				openTab('blogLink', js_translation["blogLinkTitle"], 'stimulustab2.html', false);
			activeTab = openTabs['blogLink'].index;
			break;
		}
		case 'item7': 
		case 'item8':
		{
			if (!openTabs['newsLink']) 
				openTab('newsLink', js_translation["newsLinkTitle"], 'stimulustab3.html', false);
			activeTab = openTabs['newsLink'].index;
			break;
		}
		default: {
			break;
		}
	}

	if (activeTab == null) 
		activeTab = 0;
	
	tabs.tabs({"active": activeTab});
	//setBrowserURL(tabURLs[activeTab]);


});

/*
        <li><a href="#view2">Book Review</a></li>
        <li><a href="#view3">Science News</a></li>
        <div id="view2">
            <iframe src="tab2.html" id="tabFrame2" name="tabFrame2" class="tabFrame"></iframe>
        </div>
        <div id="view3">
            <iframe src="tab3.html" id="tabFrame3" name="tabFrame3" class="tabFrame"></iframe>
        </div>
*/


function openTab(tabID, tabLabel, tabURL, activate) {
	if (!tabs) { console.log("Tabs not found!!!"); return; }
	if (typeof openTabs[tabID] != 'undefined')  {
		tabs.tabs( "option", "active", openTabs[tabID].index );
		return;
	};

	openTabs[tabID] = {id: tabID, label: tabLabel, url: tabURL, index: numTabs};
	setUserVar("R551_Tabs", openTabs);
	numTabs++;

	var tabTemplate = "<li><a href='#{href}'>#{label}</a></li>",
		li = $( tabTemplate.replace( /#\{href\}/g, "#" + tabID ).replace( /#\{label\}/g, tabLabel ) ),
		tabContentTemplate = "<div id='{href}'><iframe src='{url}' id='{href}_frame' name='{href}_frame' class='tabFrame'></iframe></div>",
		tabContentHtml = $( tabContentTemplate.replace( /\{href\}/g, tabID ).replace( /\{url\}/g, tabURL ) );

	tabs.find( ".ui-tabs-nav" ).append( li );
	$('.tabcontents').append( tabContentHtml );

	tabs.tabs( "refresh" );
	if (activate) {
		tabs.tabs( "option", "active", numTabs-1 );
	}
}

function setBrowserURL(url) {
	return; //No URL needed
console.log("setBrowserURL: " + url);
	$('#urlField').val(url);
}