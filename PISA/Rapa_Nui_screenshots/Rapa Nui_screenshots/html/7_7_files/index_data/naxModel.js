var NaxModel = (function(){
    var _instance = null;
    
    var _process_uri = null;
    var _section_id = null;
    var _cluster_id = null;
    var _unit_id = null;
    var _lang_id = null;
    var _item_id = null;
    var _config = null;
    
    var _unit_content = [];
    var _unit_scoring = [];

    var _resourceLangs = {};
    var _contexts = {};
    
    var _NaxSequencer = null;
    
    var NaxModel = function(process_uri_, unit_id_, lang_id_, config_){
        _process_uri = process_uri_;
        _section_id = '';
        _cluster_id = '';
        _unit_id = unit_id_;
        _lang_id = lang_id_;
        if (!config_)
        	_config = "unit";
        else
        	_config = config_;
        _unitNumber = null;
        _contexts[_unit_id] = {};
        _contexts["GLOBAL"] = {};
        
        _NaxSequencer = undefined;
        loadConfig();
        _section_id = getSectionID();
        _cluster_id = getClusterID();
        
        function loadConfig(){
            //invalidate lazy variable
            _item_id = null;
            _unit_content = [];
            _unit_scoring = [];
            
            $.ajax({
                url: config['PATH_UNIT'] + '/' + _unit_id + '/' + _config + '.xml',
                dataType: 'xml',
                async: false,
                success: function(document_){
                    _unit_content = document_;
                }
            });
        
            $.ajax({
                url: config['PATH_UNIT'] + '/' + _unit_id + '/' + _lang_id + '/locale/scoring.xml',
                dataType: 'xml',
                async: false,
                success: function(document_){
                    _unit_scoring = document_;
                }
            });
        }
        
        function getCurrentUnitID(){
            return _unit_id;
        }
			
        function setCurrentUnitID(unit_id_){
            _unit_id = unit_id_;
        }
        
        function getCurrentItemID(){
            if(_item_id != null) return _item_id;
            var current_item_id = util.getCookie(_process_uri + '_' + _unit_id + '_current_item_id');
            if(current_item_id == undefined){
                $.each($(_unit_content).find('items > item'), function(k,v){
                    current_item_id = $(v).attr('id');
                    return false;
                });
                setCurrentItemID(current_item_id);
            }
            return _item_id = current_item_id; 
        }
        
        function setCurrentItemID(current_item_id_){
            _item_id = current_item_id_;
            util.setCookie(_process_uri + '_' + _unit_id + '_current_item_id', current_item_id_, 1);
        }

        function performItemTransition(direction_, id){
            var unit_item_array = [];
            $.each($(_unit_content).find('items > item'), function(k,v){
                unit_item_array.push($(v).attr('id'));
            });
            var next = unit_item_array[($.inArray(getCurrentItemID(), unit_item_array) + 1)];
            var prev = unit_item_array[($.inArray(getCurrentItemID(), unit_item_array) - 1)];
            
            var isPerformed = false;
            if(direction_ == 'previous'){
                if(prev == undefined){
                    if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.Backward) == 'function')
                        _NaxSequencer.Backward();
                    else
                        alert('_NaxSequencer.Backward()');
                }
                else {
                    setCurrentItemID(prev);
                    isPerformed = true;
                }
            }
            else if(direction_ == 'next'){
                if(next == undefined){
                    if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.Forward) == 'function')
                        _NaxSequencer.Forward();
                    else
                        alert('_NaxSequencer.Forward()');
                }
                else{
                    setCurrentItemID(next);
                    isPerformed = true;
                }
            }
            else if(direction_ == 'jump'){
                var jump = unit_item_array[$.inArray(id, unit_item_array)];;
                if(jump == undefined){
                    alert('Jump: invalid id (' + id + ')');
                }
                else{
                    setCurrentItemID(jump);
                    isPerformed = true;
                }
            }
            else if(direction_ == 'breakoff'){
                if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.BreakOff) == 'function')
                    _NaxSequencer.BreakOff();
                else
                    alert('_NaxSequencer.BreakOff()');
            }
            return isPerformed;
        }
        
        function setGlobalTime(){
        	var times = $.parseJSON(util.getCookie(_process_uri + '_section_' + _section_id + '_global_times'));           
            var time = new Date().getTime();
            var initial_section, initial_unit, initial_item;
            
            if (times == undefined || times.section == undefined || times.section.section != _section_id) {
            	initial_section = initial_unit = initial_item = time;
            } else {
            	initial_section = times.section.initial;
            	if (times.unit == undefined || times.unit.unit != _unit_id) {
            		initial_unit = initial_item = time;
            	} else {
            		initial_unit = times.unit.initial;
            		if (times.item == undefined || times.item.item != getCurrentItemID()) {
            			initial_item = time;
            		} else {
            			initial_item = times.item.initial;
            		}
            	}
            }
            
            util.setCookie(_process_uri + '_section_' + _section_id + '_global_times', $.toJSON(
            	{
            	section: {
            		'section': _section_id,
					'initial': initial_section, 
					'last': time
				},
            	unit: {
            		'unit': _unit_id,
					'initial': initial_unit, 
					'last': time
				},
            	item: {
            		'item': getCurrentItemID(),
					'initial': initial_item, 
					'last': time
				}
            }), 1);
            
            return {
                'section': (time - initial_section) / 1000,
                'unit': (time - initial_unit) / 1000,
                'item': (time - initial_item) / 1000
            } 
        }
        
        function getGlobalTime(){
        	var times = $.parseJSON(util.getCookie(_process_uri + '_section_' + _section_id + '_global_times'));           
            
            return {
                'section': (times.section.last - times.section.initial) / 1000,
                'unit': (times.unit.last - times.unit.initial) / 1000,
                'item': (times.item.last - times.item.initial) / 1000
            } 
        }
        
        function synchronizeGlobalTime(){
        	return setGlobalTime();
        }
        
        function getItemMap(unit_id_){
            var item_map = {
                'items': {}
            };
            $.each($(_unit_content).find('items > item'), function(k,v){
                item_map['items'][$(v).attr('order')] = {id: $(v).attr('id'), itemName: $(v).attr('itemName'), directions: ($(v).attr('directions') ? true : false)};
            });
            return item_map;
        }
        
		function getSectionID(){
			var sectionID = null;
			if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.GetSectionID) == 'function') 
				sectionID = _NaxSequencer.GetSectionID();
			if (sectionID == null)
				sectionID = '';
			return sectionID;
		}
            
		function getClusterID(){
			var clusterID = null;
			if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.GetClusterID) == 'function') 
				clusterID = _NaxSequencer.GetClusterID();
			if (clusterID == null)
				clusterID = '';
			return clusterID;
		}
            
		function getFormID(){
			var formID = null;
			if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.GetFormID) == 'function') 
				formID = _NaxSequencer.GetFormID();
			if (formID == null)
				formID = '';
			return formID;
		}
            
        function getEventFile(event_file_){
            var event;
            $.ajax({
                url: config['PATH_UNIT'] + '/' + _unit_id + '/' + event_file_,
                dataType: 'xml',
                async: false,
                success: function(document_){
                    var factorize_array = [];
                    $.each($(document_).find('factorize'), function(k,v){
                        factorize_array.push({
                            'name': $(v).attr('name'), 
                            'value': $(v).text().trim()
                        });
                    });
                    event = {
                        'list': {
                            'type': $(document_).find('list').attr('type'), 
                            'value': $(document_).find('list').text().trim()
                        },
                        'attributes': $(document_).find('attributes').text().trim(), 
                        'factorize': factorize_array
                    };
                }
            });
            return event;
        }

        function getPreloads() {
            var preloads = [];
            $.each($(_unit_content).find('configuration > preload > file'), function(k,v){
                preloads.push($(v).text().trim());
            });
            
            return preloads;
        }
            
        function getContext(context_id_, is_global_) {
            var root = (is_global_ ? "GLOBAL" : _unit_id);
            if ((typeof _contexts[root] != "undefined") && (typeof _contexts[root][context_id_]))
                return _contexts[root][context_id_];
            else {
                var context = null;
                $.ajax({
                    url: config['PATH_SERVICE'] + '/context/getcontext.php',
                    type: 'POST',
                    async: false,
                    cache : false,
                    data: 'PROCESSURI='+encodeURIComponent($.toJSON(_process_uri))+
                    '&UNITID='+encodeURIComponent($.toJSON(_unit_id))+
                    '&CONTEXTID='+encodeURIComponent($.toJSON(context_id_))+
                    '&ISGLOBAL='+is_global_,
                    dataType: 'json',
                    success: function(context_){
                        context = context_;
                    }
                });
                _contexts[root][context_id_] = context;
                return context;
            }
        }

        function setContext(context_id_, context_data_, is_global_) {
            var root = (is_global_ ? "GLOBAL" : _unit_id);
            _contexts[root][context_id_] = context_data_;
            $.ajax({
                url: config['PATH_SERVICE'] + '/context/setcontext.php',
                type: 'POST',
                async: false,
                cache : false,
                data: 'PROCESSURI='+encodeURIComponent($.toJSON(_process_uri))+
                '&UNITID='+encodeURIComponent($.toJSON(_unit_id))+
                '&CONTEXTID='+encodeURIComponent($.toJSON(context_id_))+
                '&CONTEXTDATA='+encodeURIComponent($.toJSON(context_data_))+
                '&ISGLOBAL='+is_global_,
                dataType: 'json'
            });
        }

        function removeAllContextForProcess(process_uri_) {
            _contexts[_unit_id] = {};
            _contexts["GLOBAL"] = {};
            $.ajax({
                url: config['PATH_SERVICE'] + '/context/removeallcontextforprocess.php',
                type: 'POST',
                async: false,
                cache : false,
                data: 'PROCESSURI='+encodeURIComponent($.toJSON(process_uri_)),
                dataType: 'json'
            });
        }
        
        return {
            LoadConfig: function(){
                loadConfig();
            },
            
            FeedTrace: function(trace_){
                $.ajax({
                    url: config['PATH_SERVICE'] + '/feedtrace/feedtrace.php',
                    type: 'POST',
                    async: false,
                    data: 'PROCESSURI='+encodeURIComponent($.toJSON(_process_uri))+
                    '&TRACE='+encodeURIComponent($.toJSON(trace_)),
                    dataType: 'json'
                });
            },
            
            FeedScore: function(score_){
                if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.FeedScore) == 'function')
                    _NaxSequencer.FeedScore(score_);
            },
            
            GetContext: function(context_id_, is_global_){
                return getContext(context_id_, is_global_);
            },
            
            SetContext: function(context_id_, context_data_, is_global_){
                setContext(context_id_, context_data_, is_global_);
            },
            
            RemoveAllContextForProcess: function(process_uri_){
                removeAllContextForProcess(process_uri_);
            },
			
            ResetUnitForProcess: function(process_uri_){
                $.ajax({
                    url: config['PATH_SERVICE'] + '/authoring/resetunitforprocess.php',
                    type: 'POST',
                    async: false,
                    cache : false,
                    data: 'PROCESSURI='+encodeURIComponent($.toJSON(process_uri_)),
                    dataType: 'json'
                });
            },
            
            SetScore: function(score_){
                if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.SetScore) == 'function')
                    _NaxSequencer.SetScore(score_);
            },
            
            IsResourceAvailableInLanguage: function(unit_id_, lang_id_, type_){
                var isResourceAvailableInLanguage = false;
                if (typeof _resourceLangs[lang_id_] == "undefined")
                    _resourceLangs[lang_id_] = {};
                if (typeof _resourceLangs[lang_id_][unit_id_] == "undefined")
                    _resourceLangs[lang_id_][unit_id_] = {};
                if (typeof _resourceLangs[lang_id_][unit_id_][type_] != "undefined")
                    return _resourceLangs[lang_id_][unit_id_][type_];

                $.ajax({
                    url: config['PATH_SERVICE'] + '/common/isresourceavailableinlanguage.php',
                    type: 'POST',
                    async: false,
                    cache : false,
                    data: 'UNITID='+encodeURIComponent($.toJSON(unit_id_))+
                    '&LANGID='+encodeURIComponent($.toJSON(lang_id_))+
                    '&TYPE='+encodeURIComponent($.toJSON(type_)),
                    dataType: 'json',
                    success: function(isResourceAvailableInLanguage_){
                        isResourceAvailableInLanguage = (isResourceAvailableInLanguage_ == true | isResourceAvailableInLanguage_ == 'true') ? true : false;
                    }
                });
                _resourceLangs[lang_id_][unit_id_][type_] = isResourceAvailableInLanguage;
                return isResourceAvailableInLanguage;
            },
            
            GetUnitNumber: function(){
                var unit_number;
                if (_unitNumber != null)
                    return _unitNumber;
                if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.GetUnitNumber) == 'function')
                    unit_number = _NaxSequencer.GetUnitNumber();
                else
                    unit_number = 1;
                _unitNumber = unit_number;
                return unit_number;
            },  
            
            GetItemNumber: function(){
	        var count = 0;
                var item_number = 0;
                $.each(getItemMap(_unit_id).items, function(k,v){
                	if (!v.directions)
                		count++;
                    if(v.id == getCurrentItemID())
                        item_number = count;
                });
                return item_number;
            },  
            
            GetItemName: function(){
                var item_name = null;
                var item_name_file = $(_unit_content).find('items > item#' + getCurrentItemID()).attr('itemName');
                if(item_name_file != null)
                    item_name = item_name_file;
                return item_name;
            },  
            
            GetTotalItemNumber: function(){
                var item_number = 0;
                $.each(getItemMap(unit_id_).items, function(k,v){
                	if (!v.directions)
                		item_number++;
                });
                return item_number;
            },
			
            GetRule: function(){
                var rule;
                $.each($(_unit_scoring).find('scoring:[item='+getCurrentItemID()+']'), function(k,v){
                    rule = $(v).text();
                });
                return rule;
            },
            
            GetScoringAttributes: function(){
                var attributes = {mode: null, source: null};
                $.each($(_unit_scoring).find('scoring:[item='+getCurrentItemID()+']'), function(k,v){
                    attributes = {
                        mode: $(v).attr('mode'), 
                        source: $(v).attr('source')
                    };
                });
                return attributes;
            },
            
            GetEvent: function(){
                var all_event_file = $(_unit_content).find('items').attr('event')
                if(all_event_file != null)
                    var all_event = getEventFile(all_event_file);
                
                var item_event_file = $(_unit_content).find('items > item#' + getCurrentItemID()).attr('event')
                if(item_event_file != null)
                    var item_event = getEventFile(item_event_file);
                
                var stimulus_event_file = $(_unit_content).find('items > item#' + getCurrentItemID() + ' > stimulus').attr('event')
                if(stimulus_event_file != null)
                    var stimulus_event = getEventFile(stimulus_event_file);
                
                var question_event_file = $(_unit_content).find('items > item#' + getCurrentItemID() + ' > question').attr('event')
                if(question_event_file != null)
                    var question_event = getEventFile(question_event_file);
               
                return {
                    'all': all_event,
                    'item': item_event,
                    'stimulus': stimulus_event,
                    'question': question_event
                } 
            },
            
            GetProcessUri: function(){ 
                return _process_uri;
            },
            
            GetLang: function(){ 
                return _lang_id;
            },
            
            GetModule: function(){ 
                var unit_modules = [];
                $.each($(_unit_content).find('configuration > modules > module'), function(k,v){
                    var params = {};
                    $.each($(_unit_content).find('configuration > modules > module#' + $(v).attr('id') + ' > param'), function(k,v){
                        params[$(v).attr('id')] = $(v).text().trim();
                    });
                    if(util.objectSize(params) > 0)
                        unit_modules.push({
                            'id': $(v).attr('id'), 
                            'module': $(v).attr('module'), 
                            'params': params
                        })
                    else
                        unit_modules.push({
                            'id': $(v).attr('id'), 
                            'module': $(v).attr('module')
                        });
                });
                var item_modules = [];
                $.each($(_unit_content).find('items > item#' + getCurrentItemID() + ' > modules > module'), function(k,v){
                    var params = {};
                    $.each($(_unit_content).find('items > item#' + getCurrentItemID() + ' > modules > module#' + $(v).attr('id') + ' > param'), function(k,v){
                        params[$(v).attr('id')] = $(v).text().trim();
                    });
                    if(util.objectSize(params) > 0)
                        item_modules.push({
                            'id': $(v).attr('id'), 
                            'module': $(v).attr('module'), 
                            'params': params
                        });
                    else
                        item_modules.push({
                            'id': $(v).attr('id'), 
                            'module': $(v).attr('module')
                        });
                });
                
                return {
                    'unit': unit_modules,
                    'item': item_modules
                }
            },
            
            GetMeta: function(){ 
                var metas = {};
                $.each($(_unit_content).find('metas > meta'), function(k,v){
                    metas[$(v).attr('id')] = $(v).text().trim();
                });
                
                return metas;
            },
            
            GetCSS: function(){
                return {
                    'platform': config['PATH_UNIT']  + '/' + _unit_id + '/' + $(_unit_content).find('items > item#' + getCurrentItemID()).attr('css'),
                    'locale': config['PATH_UNIT']  + '/' + _unit_id + '/' + _lang_id + '/' + $(_unit_content).find('items > item#' + getCurrentItemID()).attr('css')
                }
            },
            
            GetCurrentStimulus: function(){ 
                var current_stimulus_file = null;
                $.each($(_unit_content).find('items > item'), function(k,v){
                    if($(v).attr('id') == getCurrentItemID()){
                        current_stimulus_file =  {
                            'url' : config['PATH_UNIT'] + '/' + _unit_id + '/' + _lang_id + '/' + $(_unit_content).find('items > item#' + $(v).attr('id') + ' > stimulus').attr('src'),
                            'reload' : (function(){
                                return ($(_unit_content).find('items > item#' + $(v).attr('id') + ' > stimulus').attr('reload') == 'false') ? false : true;
                            })()
                        }
                    }
                });
                return current_stimulus_file;
            },
            
            GetCurrentQuestion: function(){ 
                var current_question_file = null;
                $.each($(_unit_content).find('items > item'), function(k,v){
                    if($(v).attr('id') == getCurrentItemID()){
                        current_question_file = {
                            'url' : config['PATH_UNIT'] + '/' + _unit_id + '/' + _lang_id + '/' + $(_unit_content).find('items > item#' + $(v).attr('id') + ' > question').attr('src'),
                            'reload' : (function(){
                                return ($(_unit_content).find('items > item#' + $(v).attr('id') + ' > question').attr('reload') == 'false') ? false : true;
                            })()
                        }
                        return false;
                    }
                });
                return current_question_file;
            },

            GetAllFiles: function() {
                var files = [];
                $.each($(_unit_content).find('items > item'), function(k,v){
                    files.push({
                        'url' : config['PATH_UNIT'] + '/' + _unit_id + '/' + _lang_id + '/' + $(_unit_content).find('items > item#' + $(v).attr('id') + ' > question').attr('src'),
                        'type' : 'question',
                        'reload' : (function(){
                            return ($(_unit_content).find('items > item#' + $(v).attr('id') + ' > question').attr('reload') == 'false') ? false : true;
                        })()
                    });
                    files.push({
                        'url' : config['PATH_UNIT'] + '/' + _unit_id + '/' + _lang_id + '/' + $(_unit_content).find('items > item#' + $(v).attr('id') + ' > stimulus').attr('src'),
                        'type' : 'stimulus',
                        'reload' : (function(){
                            return ($(_unit_content).find('items > item#' + $(v).attr('id') + ' > stimulus').attr('reload') == 'false') ? false : true;
                        })()
                    });
                    //return false;
                });

                var preloads = getPreloads();
                for (var i in preloads) 
                    files.push({
                        'url': config['PATH_UNIT'] + '/' + _unit_id + '/' + _lang_id + '/' + preloads[i],
                        'type' : '',
                        'reload': true
                    });
                return files;
            },
            
            GetGlobalTime: function(){
                return setGlobalTime();
            },
            
            GetMap: function(){
                var mapWF = null;
                if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.GetMap) == 'function') 
                    mapWF = _NaxSequencer.GetMap();
				if (mapWF == null)
					mapWF = {
						1: _unit_id
					};
                var map = {};
                $.each(mapWF, function(key, unit_id){
                    map[unit_id] = getItemMap(unit_id);
                });
                return map;
            },
            
            GetSectionMap: function(){
                var mapWF = null;
                if(typeof(_NaxSequencer) != 'undefined' && typeof(_NaxSequencer.GetSectionMap) == 'function') 
                    mapWF = _NaxSequencer.GetSectionMap();
				if (mapWF == null)
					mapWF = {
						1: _unit_id
					};
                var map = {};
                $.each(mapWF, function(key, unit_id){
                    map[unit_id] = unit_id;
                });
                return map;
            },
            
            GetCurrentUnitID: function(){
                return getCurrentUnitID();
            },
			
            SetCurrentUnitID: function(unit_id_){
                setCurrentUnitID(unit_id_);
            },
            
            GetCurrentItemID: function(){
                return getCurrentItemID();
            },
            
            GetFormID: function(){
            	return getFormID();
            },
			
            PerformPreviousItemTransition: function(){ 
                return performItemTransition('previous');
            },
            
            PerformNextItemTransition: function(){
                return performItemTransition('next');
            },
            
            PerformJumpItemTransition: function(id){
                return performItemTransition('jump', id);
            },
            
            PerformBreakOffTransition: function(){
                return performItemTransition('breakoff');
            },
            
            SynchronizeGlobalTime: function(){
                synchronizeGlobalTime();
            }
			
        };
    };
    
    return {
        getInstance : function(_process_uri, unit_id_, lang_id_, config_){
            if(_instance == null){
                _instance = NaxModel(_process_uri, unit_id_, lang_id_, config_);
            }
            return _instance;
        }
    };
})();
