var url_vars = util.getUrlVars();

var config = {};
    
config['PATH_ROOT'] = './';
config['PATH_UNIT_ROOT'] = '../../../../pisa2018-cba/';
config['PATH_MODULE_ROOT'] = '../../../../../pisa2018-cba/';

config['SUFFIX_CSS'] = 'css';
config['SUFFIX_DATA'] = 'data';
config['SUFFIX_LIB'] = 'lib';
config['SUFFIX_UNIT'] = 'unit';
config['SUFFIX_MODULE'] = 'module';
if (url_vars['preview'] == '1')
    config['SUFFIX_UNIT'] = 'tmp';
config['SUFFIX_SERVICE'] = 'service';

config['PATH_CSS'] = config['PATH_ROOT'] + config['SUFFIX_CSS'];
config['PATH_DATA'] = config['PATH_ROOT'] + config['SUFFIX_DATA'];
config['PATH_LIB'] = config['PATH_ROOT'] + config['SUFFIX_LIB'];
config['PATH_UNIT'] = config['PATH_UNIT_ROOT'] + config['SUFFIX_UNIT'];
config['PATH_MODULE'] = config['PATH_MODULE_ROOT'] + config['SUFFIX_MODULE'];
config['PATH_SERVICE'] = config['PATH_ROOT'] + config['SUFFIX_SERVICE'];
