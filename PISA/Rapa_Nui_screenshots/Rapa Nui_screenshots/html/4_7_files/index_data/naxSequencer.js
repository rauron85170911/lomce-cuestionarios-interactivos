var NaxSequencer = (function(){
    var _instance = null;
    
    var NaxSequencer = function(){
        
        return {
            
            GetUnitNumber: function(){
                var unitNumber;
                $.ajax({
                    url: config['PATH_SEQUENCER_SERVICE'] + '/getUnitNumber.php',
                    async: false,
                    cache: false,
                    dataType: 'json',
                    success: function(data){
                        unitNumber = data;
                    }
                });
                return unitNumber;
            },
            
            SetScore: function(score_){
                $.ajax({
                    url: config['PATH_SEQUENCER_SERVICE'] + '/setScore.php',
                    type: 'POST',
                    async: false,
                    cache : false,
                    data: 'SCORE='+encodeURIComponent($.toJSON(score_))
                });
            },
            
            FeedScore: function(score_){
                $.ajax({
                    url: config['PATH_SEQUENCER_SERVICE'] + '/feedScore.php',
                    type: 'POST',
                    async: false,
                    cache : false,
                    data: 'SCORE='+encodeURIComponent($.toJSON(score_))
                });
            },
            
            Forward: function(){
                window.location.replace('http://' + window.location.host + window.location.pathname + '/../../sequencer/wf/wfItem.php');
            },
            
            BreakOff: function(){
                window.location.replace('http://' + window.location.host + window.location.pathname + '/../../sequencer/wf/wfItem.php?breakoff=1');
            }
        };
    };
    
    return {
        getInstance : function(){
            if(_instance == null){
                _instance = NaxSequencer();
            }
            return _instance;
        }
    };
})();